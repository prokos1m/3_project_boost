using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] float levelLoadDelay = 2f;
    [SerializeField] AudioClip crushSound;
    [SerializeField] AudioClip winSound;
    [SerializeField] ParticleSystem sucessParticles;
    [SerializeField] ParticleSystem crashParticles;

    AudioSource audioSource;

    bool isTransitioning = false;
    bool collisionDisabled = false;
    void Start() 
    {
        audioSource = GetComponent<AudioSource>();
         
    }
    void Update() 
    {
        ProcessCheats();
    }

    void ProcessCheats()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            DisableCollisions();
        }
    }
    void DisableCollisions()
    {
        collisionDisabled = !collisionDisabled; // Flip the value of the boolean
    }
    void OnCollisionEnter(Collision other) 
    {
        if (isTransitioning || collisionDisabled)
        {
            return;    
        }
        switch (other.gameObject.tag)
        {
            case "Friendly":
                Debug.Log("You've hit a friendly thing.");
                break;
            case "Finish":
                Debug.Log("You've hit the finish.");
                StartSuccessSequence();
                break;
            default:
                Debug.Log("You've blown up, dude!");
                StartCrashSequence();
                break;
        }
    }
    void StartSuccessSequence()
    {
        isTransitioning = true;
        audioSource.Stop();
        audioSource.PlayOneShot(winSound);
        sucessParticles.Play();
        GetComponent<Movement>().enabled = false; //You can't move after Winnong
        Invoke ("LoadNextLevel", levelLoadDelay);
    }
    void StartCrashSequence()
    {
        isTransitioning = true;
        audioSource.Stop();
        audioSource.PlayOneShot(crushSound);
        crashParticles.Play();
        GetComponent<Movement>().enabled = false;
        Invoke ("ReloadLevel", levelLoadDelay);
    }
    public void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0;
        }
        SceneManager.LoadScene(nextSceneIndex);
    }
    void ReloadLevel() 
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex; 
        SceneManager.LoadScene(currentSceneIndex);
    }
}
