using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
    Vector3 startingPosition;
    [SerializeField] Vector3 movementVector;
    //[SerializeField] [Range(0,1)] float movementFactor; // nice way to have a slider in the Editor
    float movementFactor;
    [SerializeField] float period = 2f;

    void Start()
    {
        startingPosition = transform.position;
    }

   
    void Update()
    {
        if (period <= Mathf.Epsilon) { return; } // watch out for period being 0, rather too small (Mathf.Epsilon) -> Planck's length
        
        float cycles = Time.time / period; // this grows over time
        
        const float tau = Mathf.PI * 2; // 2*PI = tau = 6.283
        float rawSineWave = Mathf.Sin(tau * cycles); // -1 to 1 raw sine function

        movementFactor = (rawSineWave + 1f) / 2f; //remapped to 0 to 1

        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPosition + offset;
        

    }
}
